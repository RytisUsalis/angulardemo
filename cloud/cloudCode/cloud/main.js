var _ = require('underscore.js');

Parse.Cloud.define("sumHistory", function (request, response) {
    var userIdList = request.params.usersId;

    //Initialize result array
    var finalResults = [];

    Parse.Promise.as().then(function () {
        // Collect one promise for each query into an array.
        var promises = [];

        for (var i = 0; i < userIdList.length; i++) {
            (function (j) {  //create new closure so i changes with each callback
                var userId = userIdList[j];
                var query = new Parse.Query("EventHistory");
                query.equalTo("submittedBy", userId);
                
                promises.push(
                  query.find({
                      success: function (results) {
                          if (results.length > 0) {
                              var sum = 0;
                              for (var s = 0; s < results.length; ++s) {
                                  sum += 1;
                              }
                              finalResults.push({ x: userId, y: [sum] });
                          }
                      },
                      error: function () {
                          response.error();
                      }
                  })
                );
            })(i);
        }
        // Return a new promise that is resolved when all of the queries are finished.
        return Parse.Promise.when(promises);
    }).then(function () {
        response.success(finalResults);
    });
});