(function () {
    'use strict';

    var app = angular.module('app');
    app.config(['$stateProvider', '$urlRouterProvider',
      function ($stateProvider, $urlRouterProvider) {
          $urlRouterProvider.otherwise('/login');

          $stateProvider.
             state('login', {
                 url: '/login',
                 templateUrl: '/partials/login.html',
                 controller: 'loginController',
             }).
              state('events', {
                  url: '/events',
                  templateUrl: '/partials/events.html',
                  controller: 'eventsController',
                  resolve: {
                      events: function (ParseServices) {
                          return ParseServices.getEvents();
                      },
                      users: function (ParseServices) {
                          return ParseServices.getUsers();
                      }
                  }
              }).
              state('users', {
                  url: '/users',
                  templateUrl: '/partials/users.html',
                  controller: 'usersController',
                  resolve: {
                      users: function (ParseServices) {
                          return ParseServices.getUsers();
                      }
                  }
              }).
              state('createEvent', {
                  url: '/createEvent',
                  templateUrl: '/partials/createEvent.html',
                  controller: 'createEventController',
                  resolve: {
                      users: function (ParseServices) {
                          return ParseServices.getUsers();
                      }
                  }
              });
          /*.
              state('editEvent.:eventId', {
                  url: '/editEvent/:eventId',
                  templateUrl: '/partials/editEvent.html',
                  controller: 'editEventController',
                  resolve: {
                      users: function (ParseServices) {
                          return ParseServices.getUsers();
                      },
                      event: function (ParseServices, $route) {
                          return ParseServices.getEvent($route.current.params.eventId);
                      },
                      lastEventHistory: function (ParseServices, $route) {
                          return ParseServices.getLastEventHistory($route.current.params.eventId);
                      }
                  }
              }).
              state('summary.:eventId', {
                  url: '/summary/:eventId',
                  templateUrl: '/partials/summary.html',
                  controller: 'summaryController',
                  resolve: {
                      users: function (ParseServices) {
                          return ParseServices.getUsers();
                      },
                      event: function (ParseServices, $route) {
                          return ParseServices.getEvent($route.current.params.eventId);
                      },
                      eventHistory: function (ParseServices, $route) {
                          return ParseServices.getEventHistory($route.current.params.eventId);
                      },
                  }
              })*/
      }]);
})();