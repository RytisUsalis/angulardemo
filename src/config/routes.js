(function () {
'use strict';

var app = angular.module('app');
app.config(['$routeProvider', '$locationProvider',
  function ($routeProvider, $locationProvider) {
      $routeProvider.
         when('/login', {
             templateUrl: '/partials/login.html',
             controller: 'loginController',
         }).
          when('/events', {
              templateUrl: '/partials/events.html',
              controller: 'eventsController',
              resolve: {
                  events: function (ParseServices) {
                      return ParseServices.getEvents();
                  },
                  users: function (ParseServices) {
                      return ParseServices.getUsers();
                  }
              }
          }).
          when('/users', {
              templateUrl: '/partials/users.html',
              controller: 'usersController',
              resolve: {
                  users: function (ParseServices) {
                      return ParseServices.getUsers();
                  }
              }
          }).
          when('/createEvent', {
              templateUrl: '/partials/createEvent.html',
              controller: 'createEventController',
              resolve: {
                  users: function (ParseServices) {
                      return ParseServices.getUsers();
                  }
              }
          }).
          when('/editEvent/:eventId', {
              templateUrl: '/partials/editEvent.html',
              controller: 'editEventController',
              resolve: {
                  users: function (ParseServices) {
                      return ParseServices.getUsers();
                  },
                  event: function(ParseServices, $route) {
                      return ParseServices.getEvent($route.current.params.eventId);
                  },
                  lastEventHistory: function(ParseServices, $route) {
                      return ParseServices.getLastEventHistory($route.current.params.eventId);
                  }
              }
          }).
          when('/summary/:eventId', {
              templateUrl: '/partials/summary.html',
              controller: 'summaryController',
              resolve: {
                  users: function (ParseServices) {
                      return ParseServices.getUsers();
                  },
                  event: function (ParseServices, $route) {
                      return ParseServices.getEvent($route.current.params.eventId);
                  },
                  eventHistory: function (ParseServices, $route) {
                      return ParseServices.getEventHistory($route.current.params.eventId);
                  },
              }
          }).
          otherwise({ redirectTo: '/login' });
      
      $locationProvider.html5Mode(true);
  }]);
})();