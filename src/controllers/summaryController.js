﻿(function () {
    'use strict';

    var serviceId = 'summaryController';
    angular.module('app').controller(serviceId, ['$scope', '$filter', '$rootScope', 'ParseServices', 'Auth', 'users', 'event', 'eventHistory', init]);

    function init($scope, $filter, $rootScope, parseService, Auth, users, event, eventHistory) {
        $scope.$watch(function () { return Auth.isLogged(); }, function (value) {
            if (value == false) {
                $location.path('/login');
            }
        });
        $scope.users = users;
        $scope.eventHistory = eventHistory;

        $scope.chartType = 'pie';
        $scope.config1 = {
            title: event.get('title'),
            tooltips: true,
            labels: true,
            mouseover: function () { },
            mouseout: function () { },
            click: function () { },
            legend: {
                display: true,
                position: 'right'
            }
        };

        var chartData = { series: [], data: [] };
        var usersId = [];

        for (var i = 0; i < $scope.eventHistory.length; i++) {
            var userId = $scope.eventHistory[i].get('submittedBy');
            if (userId != null && usersId.indexOf(userId) == -1) {
                usersId.push(userId);
            }
        }

        parseService.countSubmittedEventsByUser(usersId).then(function (result) {
            for (var i = 0; i < result.length; i++) {
                var userId = result[i].x;
                var user = parseService.getUser(userId);
                if (user != null) {
                    var userName = user.get('name') || 'No name';
                    result[i].x = userName;
                }
            }
            chartData.data = result;
            $scope.data1 = chartData;
            $rootScope.$apply();

        });
        

    };
})();