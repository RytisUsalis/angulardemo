(function () {
    'use strict';

    var serviceId = 'usersController';
    angular.module('app').controller(serviceId, ['$scope', '$routeParams', 'ParseServices', 'Auth', 'users', init]);

    function init($scope, $routeParams, parseService, Auth, users) {
        $scope.$watch(function () { return Auth.isLogged(); }, function (value) {
            if (value == false) {
                $location.path('/login');
            }
        });
        
        $scope.users = users;
        //$scope.searchQry = '';
    };

})();