﻿(function () {
    'use strict';

    var serviceId = 'createEventController';
    angular.module('app').controller(serviceId, ['$scope', 'users', '$routeParams', 'ParseServices', 'Auth', init]);

    function init($scope, users, $routeParams, parseService, Auth) {
        $scope.$watch(function () { return Auth.isLogged(); }, function (value) {
            if (value == false) {
                $location.path('/login');
            }
        });
        
        $scope.users = users;
        $scope.createEvent = createEvent;
        $scope.isExist = isExist;

        $scope.participants = [];
        $scope.addToParticipants = addToParticipants;

        $scope.getDate = new Date();
        
        function addToParticipants(id) {
            var index = $scope.participants.indexOf(id);
            if (index > -1) {
                $scope.participants.splice(index, 1);
            }
            else {
                $scope.participants.push(id);
            }
        };

        function isExist(id) {
            return $scope.participants.indexOf(id) != -1;
        };
        
        function createEvent() {
            parseService.createEvent($scope.title, $scope.eventDate, $scope.participants, function () {
                location.href = '/events';
            });
        };
    };
})();