﻿(function () {
    'use strict';

    var serviceId = 'editEventController';
    angular.module('app').controller(serviceId, ['$scope', '$routeParams', 'ParseServices', 'Auth', 'users', 'event', 'lastEventHistory', init]);

    function init($scope, $routeParams, parseService, Auth, users, event, lastEventHistory) {
        $scope.$watch(function () { return Auth.isLogged(); }, function (value) {
            if (value == false) {
                $location.path('/login');
            }
        });
        $scope.users = users;
        $scope.event = event;

        $scope.date = new Date();

        var eventId = $routeParams.eventId;
        $scope.participants = [];
        $scope.lastEventDate = "";
        if (!angular.isUndefined(lastEventHistory)) {
            $scope.participants = lastEventHistory.get('participants');
            $scope.lastEventDate = lastEventHistory.get('submitDate');
        }
            

        $scope.updateEvent = updateEvent;
        $scope.submitEvent = submitEvent;
        $scope.dellEvent = dellEvent;
        $scope.isExist = isExist;
        $scope.addToParticipants = addToParticipants;

        function addToParticipants(id) {
            var index = $scope.participants.indexOf(id);
            if (index > -1) {
                $scope.participants.splice(index, 1);
            }
            else {
                $scope.participants.push(id);
            }
        };

        function isExist(id) {
            return $scope.participants.indexOf(id) == -1;
        };

        function updateEvent(eventId) {
            parseService.updateEvent(eventId, $scope.participants, function () {
                location.href = '/events';
            });
        };
        
        function dellEvent() {
            parseService.deleteEvent(eventId, function () {
                location.href = '/events';
            });
        };
        
        function submitEvent() {
            parseService.submitEvent(eventId, $scope.participants, new Date, Parse.User.current().id, function () {
                location.href = '/events';
            });
        };
    };

})();