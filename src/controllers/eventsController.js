(function () {
    'use strict';

    var serviceId = 'eventsController';
    angular.module('app').controller(serviceId, ['$scope', '$location', 'ParseServices', 'Auth', 'events', 'users', init]);

    function init($scope, $location, parseService, Auth, events, users) {
        $scope.$watch(function () { return Auth.isLogged(); }, function (value) {
            if (value == false) {
                $location.path('/login');
            }
        });
        
        $scope.events = events;
        $scope.users = users;
        $scope.dellEvent = dellEvent;

        function dellEvent(id) {
            parseService.deleteEvent(id, function () {
                location.href = '/events';
            });
        };

    };

})();