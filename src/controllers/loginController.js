(function () {
    'use strict';

    var serviceId = 'loginController';
    angular.module('app').controller(serviceId, ['$scope', '$location', '$window', 'ParseServices', 'Auth', init]);

    function init($scope, $location, $window, parseService, Auth) {

        $scope.$watch(
            function () {
                return Auth.isLogged();
            },
            function (value) {
                if (value == false) {
                    $scope.isLogged = value;
                } else {
                    $scope.isLogged = value;
                    $location.path('/events');
                }
            });
    
        $scope.fbLogin = fbLogin;
        $scope.fbLogout = fbLogout;

        
        if (Parse.User.current() != null && Parse.User.current().get("name") != null && Parse.User.current().get("photo") != null && Parse.User.current().get("type") != null) {
            $scope.userid = Parse.User.current().id;
            $scope.username = Parse.User.current().get("first_name");
            $scope.usertype = Parse.User.current().get("type");
            $scope.profileImg = Parse.User.current().get("photo");
        }

        function permission(user_id, action) {
            parseService.activePermissions(user_id, action).then(function (result) {
                return result;
            });

        }
        
        function fbLogout() {
            parseService.fbLogout(function () {
                $scope.username = null;
                $scope.profileImg = null;
                Auth.setUser(null);
                $window.sessionStorage["user"] = null;
            });
        };

        function fbLogin() {
           if (Parse.User.current() == null) {
               parseService.fbLogin(
                    function () {
                        $scope.userid = Parse.User.current().id;
                        $scope.username = Parse.User.current().get("first_name");
                        $scope.usertype = Parse.User.current().get("type");
                        $scope.profileImg = Parse.User.current().get("photo");
                        

                        Auth.setUser(Parse.User.current());
                        $window.sessionStorage["user"] = JSON.stringify(Parse.User.current());
                    }
                );
            } else {
                $scope.userid = Parse.User.current().id;
                $scope.username = Parse.User.current().get("first_name");
                $scope.usertype = Parse.User.current().get("type");
                $scope.profileImg = Parse.User.current().get("photo");
                

                Auth.setUser(Parse.User.current());
                $window.sessionStorage["user"] = JSON.stringify(Parse.User.current());
            }
        };    

    };


})();