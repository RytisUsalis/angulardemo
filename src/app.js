(function () {
    'use strict';
    var app = angular.module('app', [
        // Angular modules 
        'ngRoute',
        //'ui.router',
        'ngResource',
        'ui.bootstrap',
        'angularCharts',
        'ui.bootstrap.datepicker',
        'ui.bootstrap.timepicker'
    ]);


    app.run(['$rootScope', '$window', '$location', 'Auth', function ($rootScope, $window, $location, Auth) {

	    $rootScope.$on('$routeChangeStart', function (currRoute, prevRoute) {
	        if (!Auth.isLogged()) {
	            $location.path('/login');
	        }
	    });

	    $window.fbAsyncInit = function () {
	        Parse.FacebookUtils.init({
	            appId: '592765334200390',
	            channelUrl: 'channel.html',
	            status: true,
	            cookie: true,
	            xfbml: true
	            });
	        };
        
	    (function (d) {
	        // load the Facebook javascript SDK
	        var jssdk,
            id = 'facebook-jssdk',
            ref = d.getElementsByTagName('script')[0];

	        if (d.getElementById(id)) {
	            return;
	        }
	        jssdk = d.createElement('script');
	        jssdk.id = id;
	        jssdk.async = true;
	        jssdk.src = "//connect.facebook.net/en_US/all.js";

	        ref.parentNode.insertBefore(jssdk, ref);

	    }(document));
	}]);
})();