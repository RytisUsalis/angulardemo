(function () {
    'use strict';
    var serviceId = 'ParseServices';
    angular.module('app').factory(serviceId, ['$q', '$rootScope', init]);

    function init($q, $rootScope) {
        Parse.initialize("w9cKTci5XIeGaHKLJfqBfFpWHF2LaYiVQ9xbFOTW", "A7j3JJXsPb3Yf3iklbVTpLDJ5G6VeCTPnF94lgaG");
        
        var User = Parse.Object.extend("_User");
        var users = null;
        var Event = Parse.Object.extend("Event");
        var EventHistory = Parse.Object.extend("EventHistory");

        var Permissions = Parse.Object.extend("Permissions");

        function countSubmittedEventsByUser(usersIdArray) {
            var task = new Parse.Promise();

            Parse.Cloud.run('sumHistory', { usersId: usersIdArray }, {
                success: function (result) {
                    task.resolve(result);
                    //callback(result);
                    //console.log(result);
                },
                error: function (error) {
                }
            });
            return task;
        }

        function usersToHash(usersArray) {
            var result = {};
            for (var i = 0; i < usersArray.length; i++) {
                result[usersArray[i].id] = usersArray[i];
            }
            return result;
        }

        function fbLogin(callback) {
            FB.getLoginStatus(function (response) {
                if (response.status == 'connected') {
                    // the user is logged in and has authenticated your
                    // app, and response.authResponse supplies
                    // the user's ID, a valid access token, a signed
                    // request, and the time the access token 
                    // and signed request each expire
                    //var uid = response.authResponse.userID;
                    //var accessToken = response.authResponse.accessToken;
                    callback();
                }/*
                else if (response.status == 'not_authorized') {
                    // the user is logged in to Facebook, 
                    // but has not authenticated your app
                }*/
                else {
                    Parse.FacebookUtils.logIn(null, {
                        success: function (user) {
                            if (!user.existed()) {
                                // Vartotojas uzregistruotas ir prijungtes prie fb
                                FB.api('/me', function (response) {
                                    user.set("name", response.name);
                                    user.set("first_name", response.first_name);
                                    user.set("last_name", response.last_name);
                                    user.save();

                                    FB.api('/me/picture?height=100&type=square&width=100', function (r) {
                                        user.set("photo", r.data.url);
                                        user.save().then(function () {
                                            callback(user);
                                        }
                                        );
                                    });
                                });

                            } else {
                            // Vartotojas prijungtes prie fb (jau uzregistruotas)
                            callback();
                            }

                        },
                        error: function (error) {
                        }
                    });
                }
            });
            
            
            
        }

        function fbLogout(callback) {
            Parse.User.logOut();
            callback('/login');
        }

        function getUsers() {
            var task = $q.defer();

            if (users != null) {
                task.resolve(users);
                return task.promise;
            }

            var query = new Parse.Query(User);
            query.find({
                success: function (results) {
                    users = usersToHash(results);
                    task.resolve(results);
                    $rootScope.$apply();
                },
                error: function (error) {
                    task.reject(error);
                }
            });
            return task.promise;
        }
        function getUser(id) {
            return users[id];
        }

        function createEvent(title, eventDate, participants, callback) {
            alert('test 2');
            var object = new Event();

            object.set('title', 'Pavyko');
            //object.set('eventDate', 'Feb 10, 2015, 10:10');
            object.save(null, {
                success: function (object) {
                    createEventHistory(object.id, participants);
                    if (callback != null) {
                        callback();
                    }
                },
                error: function (error) {
                    //console.log("Error: " + error.message);
                }
            });
        }

        function createEventHistory(eventId, participants, submitDate, submitedBy) {
            var eventHistory = new EventHistory();
            eventHistory.set('participants', participants);
            eventHistory.set('eventId', eventId);
            eventHistory.set("submitDate", submitDate);
            eventHistory.set("submittedBy", submitedBy);
            eventHistory.save(null, {
                success: function (object) {
                    
                },
                error: function (error) {
                    //console.log("Error: " + error.message);
                }
            });
        }

        function getEvents() {
            var task = $q.defer();
            var query = new Parse.Query(Event);

            query.find({
                success: function (results) {
                    task.resolve(results);
                    $rootScope.$apply();
                },
                error: function (error) {
                    task.reject(error);
                }
            });
            return task.promise;
        }

        function getEvent(id) {
            var task = $q.defer();

            var query = new Parse.Query(Event);
            query.equalTo("objectId", id);
            query.first({
                success: function (results) {
                    task.resolve(results);
                    $rootScope.$apply();
                },
                error: function (error) {
                    task.reject(error);
                }
            });
            return task.promise;
        }
        function getLastEventHistory(id) {
            var task = $q.defer();
            var query = new Parse.Query(EventHistory);

            query.equalTo("eventId", id);
            query.descending("createdAt");

            query.first({
                success: function (results) {
                    task.resolve(results);
                    $rootScope.$apply();
                },
                error: function (error) {
                    task.reject(error);
                }
            });
            return task.promise;
        }
        function getEventHistory(id) {
            var task = $q.defer();
            
            var query = new Parse.Query(EventHistory);
            query.equalTo("eventId", id);
            query.find({
                success: function (results) {
                    task.resolve(results);
                    $rootScope.$apply();
                },
                error: function (error) {
                    task.reject(error);
                }
            });
            return task.promise;
        }

        function updateEvent(id, participants, callback) {
            var query = new Parse.Query(EventHistory);
            query.equalTo("eventId", id);
            query.equalTo("submittedBy", null);
            query.first({
                success: function (results) {
                    if (results == null) {
                        createEventHistory(id, participants, null, null);
                    } else {
                        results.set("participants", participants);
                        results.save();
                    }
                    callback();
                },
                error: function (error) {
                }
            });
        }

        function deleteEvent(id, callback) {
            alert('dell');
            /*
            var query = new Parse.Query(Event);
            query.equalTo("objectId", id);
            query.destroy({
                success: function (query) {
                    // The object was deleted from the Parse Cloud.
                    callback();
                },
                error: function (query, error) {
                    // The delete failed.
                    // error is a Parse.Error with an error code and message.
                }
            });
            */
        }
        
        function submitEvent(id, participants, submitDate, submitedBy, callback) {
            var event = new Parse.Query(EventHistory);
            event.equalTo("eventId", id);
            event.equalTo("submittedBy", null);
            event.first({
                success: function (results) {
                    if (results == null) {
                        createEventHistory(id, participants, submitDate, submitedBy);
                    } else {
                        results.set("submitDate", submitDate);
                        results.set("submittedBy", submitedBy);
                        results.save();
                    }
                    callback();
                },
                error: function (error) {
                    //console.log(error);
                }
            });
        }

        function activePermissions(user_id, action) {
            var task = $q.defer();
            // pagal du key (userid ir action) gauti permission atsakyma is Permisions lenteles
            var query = new Parse.Query(Permissions);
            query.equalTo("user_id", user_id);
            query.equalTo("action", action);
            query.get('permission').then(function (result) {
                task.resolve(result);
            });
            alert(task);
            return task;
        }


        var service = {
            name: "Parse",
            fbLogin: fbLogin,
            fbLogout: fbLogout,
            getUsers: getUsers,
            getUser: getUser,
            createEvent: createEvent,
            createEventHistory: createEventHistory,
            getEvents: getEvents,
            getEvent: getEvent,
            getLastEventHistory: getLastEventHistory,
            getEventHistory: getEventHistory,
            updateEvent: updateEvent,
            submitEvent: submitEvent,
            countSubmittedEventsByUser: countSubmittedEventsByUser,
            activePermissions: activePermissions
        };
        return service;
    }
})();