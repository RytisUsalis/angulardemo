﻿(function () {
    'use strict';
    var serviceId = 'Auth';
    angular.module('app').factory(serviceId, ['$window', init]);

    function init($window) {
        var user = null;
        if ($window.sessionStorage["user"]) {
            user = JSON.parse($window.sessionStorage["user"]);
        }
        
        function setUser(aUser) {
            user = aUser;
            return user;
        }
        function isLogged() {
            return (user != null) ? true : false;
        }

        var service = {
            name: "Auth",
            setUser: setUser,
            isLogged: isLogged,
        };
        return service;
    }
})();